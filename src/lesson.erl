%%%-------------------------------------------------------------------
%%% @author kv
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 06. Sep 2016 0:05
%%%-------------------------------------------------------------------
-module(lesson).
-author("kv").

-import(default, [first/1, second/1, last/1, butlast/1, rest/1, inc/1, dec/1, powi/2]).

%% API
-export([main/0]).

main() -> binary2decimal([1,0,1,1,0]),
          perfectNumbers(500), %% must be 6,28,496...
          bubbleSort([3,8,6,1,2,3]),
          mergeSort([3,8,6,1,2,3]),
          hanoi(3).

foldil(_, Acc, [], _) -> Acc;
foldil(Fun, Acc, [H|T], Index) ->
  foldil(Fun, Fun(H, Index, Acc), T, inc(Index)).

foldil(Fun, Acc, List) ->
  foldil(Fun, Acc, List, 0).

binary2decimal(List) -> foldil(
  fun(X, Index, Acc) -> Acc + X * (powi(2, Index)) end,
  0,
  lists:reverse(List)
).

getDividers(N) ->
  [X || X <- lists:seq(1,N-1), (N rem X) =:= 0].

perfectNumbers(N) ->
  [X || X <- lists:seq(2, N), X =:= lists:sum(getDividers(X))].

bubbleSort([]) -> [];
bubbleSort([A]) -> [A];
bubbleSort([H1|T1 = [H2|T2]]) ->
  Iter =
    if
      H1 > H2 ->
        [H2] ++ bubbleSort([H1|T2]);
      true ->
        [H1] ++ bubbleSort(T1)
    end,
  bubbleSort(butlast(Iter)) ++ [last(Iter)].

mergeSort([]) -> [];
mergeSort([A]) -> [A];
mergeSort(List) ->
  Len = lists:flatlength(List),
  {L, R} = lists:split(Len div 2, List),
  lists:merge(mergeSort(L), mergeSort(R)).

hanoi(N) -> hanoi(N, 1, 3, 2).
hanoi(1, From, To, _) -> io:fwrite("Move from ~p to ~p ~n", [From, To]), 0;
hanoi(N, From, To, Other) -> hanoi(dec(N), From, Other, To), hanoi(1, From, To, Other), hanoi(dec(N), Other, To, From).
