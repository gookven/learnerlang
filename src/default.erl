%%%-------------------------------------------------------------------
%%% @author kv
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 06. Sep 2016 0:03
%%%-------------------------------------------------------------------
-module(default).
-author("kv").

%% API
-compile(export_all).

first(A) -> hd(A).
rest(A) -> tl(A).
second([_,S|_]) -> S.
last(List) -> first(lists:reverse(List)).

butlast([]) -> [];
butlast([_]) -> [];
butlast([H|T]) -> [H|butlast(T)].

inc(X) -> X+1.
dec(X) -> X-1.

powi(_,0) -> 1;
powi(X,1) -> X;
powi(X,Y) -> B = powi(X, Y div 2), B * B * powi(X, Y rem 2).



